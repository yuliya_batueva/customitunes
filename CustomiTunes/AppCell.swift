//
//  AppCell.swift
//  CustomiTunes
//
//  Created by Julie on 20.04.17.
//  Copyright © 2017 Julie. All rights reserved.
//

import UIKit

protocol AppCellDelegate {
  func pauseTapped(_ cell: AppCell)
  func resumeTapped(_ cell: AppCell)
  func cancelTapped(_ cell: AppCell)
  func downloadTapped(_ cell: AppCell)
}

class AppCell: UITableViewCell {
  
    var delegate: AppCellDelegate?
    
    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var priceButton: UIButton!
}
