//
//  SearchViewController.swift
//  CustomiTunes
//
//  Created by Julie on 20.04.17.
//  Copyright © 2017 Julie. All rights reserved.
//

import UIKit
import MediaPlayer

class SearchViewController: UIViewController {
    
    let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    var dataTask: URLSessionDataTask?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

  var searchResults = [App]()
  
  lazy var tapRecognizer: UITapGestureRecognizer = {
    var recognizer = UITapGestureRecognizer(target:self, action: #selector(SearchViewController.dismissKeyboard))
    return recognizer
  }()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func updateSearchResults(_ data: Data?) {
    searchResults.removeAll()
    do {
      if let data = data, let response = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions(rawValue:0)) as? [String: AnyObject] {

        if let array: AnyObject = response["results"] {
          for trackDictonary in array as! [AnyObject] {
            if let trackDictonary = trackDictonary as? [String: AnyObject]{
                
              let name = trackDictonary["trackName"] as? String
              let image = trackDictonary["artworkUrl60"] as? String
                let price = trackDictonary["formattedPrice"] as? String
                let date = trackDictonary["currentVersionReleaseDate"] as? String
                let version = trackDictonary["version"] as? String
                let id = trackDictonary["trackViewUrl"] as? String
                searchResults.append(App(name: name, image: image, version: version
                    , date: date, price: price, id: id))
            } else {
              print("Not a dictionary")
            }
          }
        } else {
          print("Results key not found in dictionary")
        }
      } else {
        print("JSON Error")
      }
    } catch let error as NSError {
      print("Error parsing results: \(error.localizedDescription)")
    }
    
    DispatchQueue.main.async {
      self.tableView.reloadData()
      self.tableView.setContentOffset(CGPoint.zero, animated: false)
    }
  }
  
  // MARK: Keyboard dismissal
  
  func dismissKeyboard() {
    searchBar.resignFirstResponder()
  }
  
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
        
        if !searchBar.text!.isEmpty {
            
            if dataTask != nil {
                dataTask?.cancel()
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let expectedCharSet = NSCharacterSet.urlQueryAllowed
            let searchTerm = searchBar.text!.addingPercentEncoding(withAllowedCharacters: expectedCharSet)
            
            let url = NSURL(string: "https://itunes.apple.com/search?media=software&entity=software&term=\(searchTerm!)")
            
            dataTask = defaultSession.dataTask(with: url! as URL) {
                data, response, error in
                
                DispatchQueue.main.async() {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                
                if let error = error {
                    print(error.localizedDescription)
                } else if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        
                        self.updateSearchResults(data)
                    }
                }
            }
            
            dataTask?.resume()
        }
    }
    
  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
    
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    view.addGestureRecognizer(tapRecognizer)
  }
    
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    view.removeGestureRecognizer(tapRecognizer)
  }
}


// MARK: UITableViewDataSource

extension SearchViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchResults.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "AppCell", for: indexPath) as! AppCell
    
    
    let track = searchResults[indexPath.row]
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    cell.titleLabel.text = track.name
    var dateObj = dateFormatter.date(from: "")
    if let date = track.date {
        dateObj = dateFormatter.date(from: date)
    } else {
        print("date is nil")
    }
    dateFormatter.dateFormat = "dd-MM-yyyy"
    if let dateObjString = dateObj {
        cell.dateLabel.text = dateFormatter.string(from: dateObjString)
    }
    if let image = track.image, let url = URL(string: image), let data = try? Data(contentsOf: url as URL){
        cell.iconView.image = UIImage(data: data as Data)
    }

    if let price = track.price {
        cell.priceButton.setTitle(price, for: .normal)
    } else {
        cell.priceButton.setTitle("Free", for: .normal)
    }
    cell.priceButton.addTarget(self, action: #selector(priceButtonTapped(sender:)), for: .touchUpInside)
    cell.priceButton.tag = indexPath.row
    cell.versionLabel.text = track.version
    
    return cell
  }
    
    func priceButtonTapped(sender: UIButton!) {
        let track = searchResults[sender.tag]
        if let trackId = track.id, let url = URL(string: trackId) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }

}

// MARK: UITableViewDelegate

extension SearchViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 112.0
  }
}

