//
//  App.swift
//  CustomiTunes
//
//  Created by Julie on 20.04.17.
//  Copyright © 2017 Julie. All rights reserved.
//

class App {
    var name: String?
    var image: String?
    var version: String?
    var date: String?
    var price: String?
    var id: String?
    
    
    init(name: String?, image: String?, version: String?, date: String?, price: String?, id: String?) {
        self.name = name
        self.image = image
        self.version = version
        self.date = date
        self.price = price
        self.id = id
    }
}
